'''
Created on 17.04.2013

@author: Farid
'''

from models.matrix_utils import MatrixUtils
from models.algorithm import Algorithm
from test_tables import test_table_4
from test_tables import test_table
import cProfile
    

def linear_split_test():
#    for _ in range(100):
    print len(MatrixUtils.linearSplittedMatricesFromValues(test_table_4, 5))


def rect_split_test():
#    for _ in range(100):
    print len(MatrixUtils.rectSplitedMatricesFromValues(test_table_4, 5, 5))
    
def recurtion_test():
    for _ in range(100):
        Algorithm.applyMatrixRecurtion(MatrixUtils.matrixFromListValues(test_table))
    
def first_line_or_test():
    for _ in range(100):
        Algorithm.applyMatrixFirstLineOR(MatrixUtils.matrixFromListValues(test_table))

    
if __name__ == '__main__':
#    cProfile.run('rect_split_test()')
#    cProfile.run('linear_split_test()')
#    cProfile.run('first_line_or_test()')
#     cProfile.run('recurtion_test()')
#    lines = Algorithm.applyMatrixRecurtion(MatrixUtils.matrixFromListValues(test_table))
    lines = Algorithm.applyMatrixRecurtion(MatrixUtils.matrixFromListValues(test_table))
    for line in lines:
        print line

    