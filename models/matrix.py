'''
Created on 25.04.2013

@author: Farid
'''

from line import Line

class Matrix(object):
    
    def __init__(self, lines = [], index = [0, 0]):
        self.lines = lines
        self.linesMapping = {}
        self.index = index
    

    
    def sort_by_ones_qnty(self):
        ''' returns an array of pairs:
            first is the previous index
            second goes the array it self
        '''
        from matrix_utils import MatrixUtils
        self.lines.sort(MatrixUtils.reverseLines)

############################################################

    def split(self, max_lines=25):
        from matrix_utils import MatrixUtils
        splited_lines = MatrixUtils.split_array(self.lines, max_lines)
        matrixes = []
        for lines in splited_lines:
            matrixes.append(Matrix(lines))
        
        return matrixes

############################################################
    
    def updateLinesMapping(self):
        for key, value in enumerate(self.lines):
            self.linesMapping[value.index] = key

############################################################

    def lineForIndex(self, index):
        return self.lines[self.linesMapping[index]]

############################################################

    def binary_or_for_first_line(self):
        first_line = self.lines[0].deepcopy()
        iter_lines = iter(self.lines)
        next(iter_lines)
        
        try:
            passedLoop = False
            while first_line.is_full_of_zeros():
                first_line = next(iter_lines)
                passedLoop = True
            if passedLoop:
                first_line = first_line.deepcopy()
                
        except StopIteration:
            return first_line
        
        for line in iter_lines:
            first_line.binary_or(line)

        return first_line

############################################################

    def binary_or_for_first_line_with_full_check(self):
        if not len(self.lines):
            return Line()
        first_line = self.lines[0].deepcopy()
        iter_lines = iter(self.lines)
        next(iter_lines)
        
        try:
            passedLoop = False
            while first_line.is_full_of_zeros():
                first_line = next(iter_lines)
                passedLoop = True
            if passedLoop:
                first_line = first_line.deepcopy()
                
        except StopIteration:
            return first_line
             
        for line in iter_lines:
            if line == first_line:
                continue
            first_line.binary_or(line)
            if first_line.is_full_of_ones():
                break
            
        return first_line

############################################################

    def binary_full_check_reverse(self):
        line = self.binary_or_for_first_line_with_full_check()
        result_lines = []
        result_lines.append(line)
        
        for value in line.indexes:
            newLine = line.deepcopy()
            newLine.binary_xor(self.lineForIndex(value))
            result_lines.append(newLine)
        
        return result_lines

############################################################
    
    def _binary_or_for_first_line_expect(self, line_index):
        first_line = self.lines[0].deepcopy()
        counter = 0
        
        for line in self.lines:
            if counter != line_index:
                first_line.binary_or(line)
            
            counter += 1
            
        return first_line
    
############################################################
    
    def __str__(self):
        description = 'index: ' + str(self.index) + '\n'
        for line in self.lines:
            description += line.__str__() + '\n'
        
        return description

