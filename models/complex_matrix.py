'''
Created on 02.05.2013

@author: Farid
'''


class ComplexMatrix(object):

    def __init__(self, matrices):
        self.matrices   = matrices
        self.columns    = len(self.matrices[0].lines)
        self.rows       = len(self.matrices) / self.columns
        
    
    def matrixAtRowColumn(self, row, column):
        return self.matrices[row * self.columns + column]
    
    
    
