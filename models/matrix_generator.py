'''
Created on 17.05.2013

@author: Farid
'''

import random
from matrix_utils import MatrixUtils

class MatrixGenerator(object):

    @staticmethod
    def generateMatrix(rows, columns, max_density):
        '''
        
        :param rows: integer
        :param columns: integer
        :param density: one-unit quantity in percentage
        '''
        ones_distribution = rows * columns * max_density / 100
        raw_matrix = []
        for _ in range(rows):
            raw_row = []
            for _ in range(columns):
                raw_row.append(0)
            
            raw_matrix.append(raw_row)
            
        pretty_matrix = MatrixUtils.matrixFromListValues(raw_matrix)
        
        for _ in range(ones_distribution):
            random_row = random.randint(0, rows - 1)
            random_column = random.randint(0, columns - 1)
            random_line = pretty_matrix.lines[random_row]
            random_line.values[random_column] = 1
            
        return pretty_matrix
