'''
Created on 25.04.2013

@author: Farid
'''


from line import Line
from matrix import Matrix
from complex_matrix import ComplexMatrix


class MatrixUtils(object):

    @staticmethod
    def split_array(array, divisions):
        return [array[i:i+divisions] for i in range(0, len(array), divisions)]
        
############################################################

    @staticmethod
    def complexMatrixFromValues(values, horiz_max_lines = 10, vert_max_lines = 10):
        matrices = MatrixUtils.rectSplitedMatricesFromValues(values,
                                                         horiz_max_lines,
                                                         vert_max_lines)
         
        return ComplexMatrix(matrices)
    
############################################################

    @staticmethod
    def linearSplittedMatricesFromValues(values, max_lines = 25):
        matrix = MatrixUtils.matrixFromListValues(values)
        splited_lines = MatrixUtils.split_array(matrix.lines, max_lines)
        matrixes = []
        for lines in splited_lines:
            matrixes.append(Matrix(lines))
        
        return matrixes
    
############################################################

    @staticmethod
    def rectSplitedMatricesFromValues(values, horiz_max_lines = 10, vert_max_lines = 10):
        horiz_splitted_lines = []
        for hrz_line in values:
            horiz_splitted_lines.append(MatrixUtils.split_array(hrz_line, horiz_max_lines))    
        
        result = []
        final_results = []
        elements_count = len(horiz_splitted_lines[0])
         
        for _ in range(elements_count):
            result.append([])
        
        line_count = 0
        for line in horiz_splitted_lines:
            element_count = 0
            for element in line:
                result[element_count].append(element)
                element_count += 1
                
            line_count += 1
            
            if (not (line_count % horiz_max_lines) and line_count):
                final_results.append(result)
                result = []
                for _ in range(elements_count):
                    result.append([])
                
        matrices = []
        mat_counter = 0    
        for res in final_results:
            for val in res:
                mat_index = [mat_counter / horiz_max_lines, mat_counter % vert_max_lines]
                matrices.append(Matrix(MatrixUtils.matrixLinesFromListValues(val), mat_index))
                mat_counter += 1
        
        return matrices
    
############################################################

    @staticmethod
    def matrixLinesFromListValues(values):
        lines = []
        for key, values in enumerate(values):
            lines.append(Line(key, indexes = [], values = values))
            
        return lines
    
############################################################
    
    @staticmethod   
    def matrixFromListValues(values):
        matrix = Matrix()
        for key, values in enumerate(values):
            matrix.lines.append(Line(key, indexes = [], values = values))
        
        return matrix

############################################################
    
    ''' line comparators '''
    @staticmethod
    def compareLines(_1, _2):
        return _1.ones_qnty() - _2.ones_qnty()
    
############################################################
    
    @staticmethod
    def reverseLines(_1, _2):
        return _2.ones_qnty() - _1.ones_qnty()

############################################################

