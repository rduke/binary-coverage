'''
Created on 02.05.2013

@author: Farid
'''

from matrix import Matrix
from line import Line

class Algorithm(object):
    '''
    classdocs
    '''
    
    @staticmethod
    def applyMatrixFirstLineOR(matrix):
        return matrix.binary_or_for_first_line_with_full_check()
    
############################################################

    @staticmethod
    def applyMatrixSplit(matrix):
        matrixes = matrix.split(5)
        ored_lines = []
        is_done = False
        
        for mat in matrixes:
            line = mat.binary_or_for_first_line()
            
            if not len(line.values):
                continue
            
            if not line.is_full_of_zeros():
                ored_lines.append(line)
            else:
                continue
                
            if line.is_full_of_ones():
                is_done = True
                break
        
        if not is_done:
            if not len(ored_lines):
                return Line()
            
            new_matrix = Matrix(ored_lines)
    
            return new_matrix.binary_or_for_first_line_with_full_check()
    
############################################################

    @staticmethod
    def applyMatrixRecurtion(matrix):
        matrix.sort_by_ones_qnty()
        matrix.updateLinesMapping()
    
        return matrix.binary_full_check_reverse()

############################################################    
    
    @staticmethod
    def applyComplexMatrixBinaryOR(complex_matrix):
        columns = []
        for column in range(complex_matrix.columns - 1):
            print 'column: ' + str(column)
            for row in range(complex_matrix.rows - 1):
                matrix = complex_matrix.matrixAtRowColumn(row, column)
                line = Algorithm.applyMatrixFirstLineOR(matrix)
                if not(not len(line.values) or line.is_full_of_zeros()):
                    columns.append(matrix)

        return columns
        
        