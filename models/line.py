'''
Created on 25.04.2013

@author: Farid
'''

class Line(object):
    
    def __init__(self, index = 0, indexes = [], values = [], ones_count = []):
        self.index      = index
        self.indexes    = indexes
        self.values     = values
        self.ones_count = ones_count
        if len(self.values) != len(self.ones_count):
            self.ones_count.extend(values)

############################################################
    
    def ones_qnty(self):
        qnty = 0
        for value in self.values:
            if value:
                qnty += 1
        
        return qnty

############################################################

    def is_full_of_ones(self):
        return self.ones_qnty() == len(self.values)

############################################################

    def is_full_of_zeros(self):
        return self.ones_qnty() == 0;

############################################################

    def binary_or(self, line):
        before_ones_qnty = self.ones_qnty()
        for key, value in enumerate(line.values):
            self_value = self.values[key]
            self.values[key] = self_value | value
            self.ones_count[key] += value
        
        if self.ones_qnty() > before_ones_qnty:    
            for index in line.indexes:
                self.indexes.append(index)
        
            self.indexes.append(line.index)
            return True
        
        return False

############################################################
    
    def binary_xor(self, line):
        for key, value in enumerate(line.values):
            self.ones_count[key]    -= value 
            if self.ones_count[key] <= 0:
                self.ones_count[key] = 0
                self.values[key] = 0
            else:    
                self.values[key]        = 1
        
        self.indexes.remove(line.index)

############################################################
    
    def __str__(self):
        return ('index      :'   + str(self.index)           + '\n' +
                'indexes    :'   + self.indexes.__str__()    + '\n' + 
                'values     :'   + self.values.__str__()     + '\n' +
                'ones count :'   + self.ones_count.__str__())
    
############################################################

    def deepcopy(self):
        idx = []
        idx.extend(self.indexes)
        
        vls = []
        vls.extend(self.values)
        
        ones = []
        ones.extend(self.ones_count)
        
        return Line(self.index,
                    indexes     = idx,
                    values      = vls,
                    ones_count  = ones)
