'''
Created on Mar 14, 2014

@author: dahirifarid
'''

from line import Line
import random

class ChromosomeLine(Line):
    '''
    classdocs
    '''
    def pointCrossover(self, chromosomeLine, index = 0):
        self.values[index] = chromosomeLine.values[index]
    
    def twoPointsCrossover(self, chromosomeLine, index1, index2):
        self.values[index1] = chromosomeLine.values[index1]
        self.values[index1] = chromosomeLine.values[index2]
    
    def uniformCrossover(self, chromosomeLine):
        linesQnty = len(self.values)
        randomQnty = random.randint(0, linesQnty)
        for _ in range(randomQnty):
            randomIndex = random.randint(0, linesQnty - 1)
            self.values[randomIndex] = chromosomeLine.values[randomIndex]
    
    def arithmeticCrossover(self, chromosomeLine):
        for key, value in enumerate(chromosomeLine.values):
            self_value = self.values[key]
            self.values[key] = self_value & value
            
    def mutationAtIndex(self, index):
        self.values[index] = not self.values[index]
        